import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JFrame;

public class GameScreen extends JFrame {
	private static final long serialVersionUID = 1761161774538424080L;
	public final int ROWS = 40, COLUMNS = 25;
	public final int WIDTH = ROWS * 15, HEIGHT = COLUMNS * 15;
	private final Color BACKGROUND_COLOR = Color.white;
	private final int START_TIME = 1000;
	int time;
	boolean dead;
	Snake sn;

	public GameScreen(String str) {
		super(str);
		HighScores.writeScores(HighScores.sortScores(HighScores.readScores()));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		getContentPane().setPreferredSize(new Dimension(WIDTH, HEIGHT + 10));
		getContentPane().setBackground(BACKGROUND_COLOR);
		pack();
		setVisible(true);
		dead = false;
	}

	public static void main(String[] args) {
		GameScreen gs = new GameScreen("Platformer");
		new ButtonPanel(gs);
		gs.sn = gs.addSnake();
		gs.play();
	}

	public void play() {
		while (true) {
			repaint();
			dead = false;
			time = START_TIME;
			while (!dead) {
				sn.act();
				pause(time / 10);
				if (time > START_TIME / 2) {
					time--;
				}
				requestFocus();
			}
			HighScores.checkScore(sn.myScoreKeeper.getScore(), this);
			repaint();
			while (dead) {
				pause(1);
			}
			sn.reset();
		}
	}

	public Snake addSnake() {
		Snake sn = (Snake) getContentPane().add(new Snake(this));
		validate();
		return sn;
	}

	public void pause(int ms) {
		try {
			Thread.sleep(ms);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void paint(Graphics g) {
		paintComponents(g);
		if (dead) {
			g.setColor(Color.BLACK);
			g.drawString("You died!", WIDTH / 2 - 40, HEIGHT / 2);
			g.drawString("You died!", WIDTH / 2 - 41, HEIGHT / 2);
		}
	}

	public Rectangle getGameBounds() {
		return new Rectangle(0, 0, WIDTH, HEIGHT - 30);
	}
}
