import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class ButtonPanel extends JPanel {
	private static final long serialVersionUID = -4505335632757622880L;
	GameScreen myFrame;

	public ButtonPanel(GameScreen gs) {
		setLayout(new FlowLayout());
		myFrame = gs;
		myFrame.add(this, BorderLayout.SOUTH);
		myFrame.validate();
		setBackground(Color.LIGHT_GRAY);
		add(new RestartButton(myFrame));
		add(new HighScoresButton(myFrame));
	}

	private class RestartButton extends JButton {
		private static final long serialVersionUID = 8566545575309765775L;
		GameScreen myFrame = null;

		public RestartButton(GameScreen gs) {
			setText("RESTART");
			setPreferredSize(new Dimension(150, 30));
			myFrame = gs;
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					myFrame.sn.reset();
					myFrame.dead = false;
				}
			});
		}
	}

	private class HighScoresButton extends JButton {
		private static final long serialVersionUID = 8566545575309765775L;
		GameScreen myFrame = null;

		public HighScoresButton(GameScreen gs) {
			myFrame = gs;
			setText("High Scores");
			setPreferredSize(new Dimension(150, 30));
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					HighScores.displayHighScores(myFrame);
				}
			});
		}
	}

}
