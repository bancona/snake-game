public class Score implements Comparable<Score> {
	public String name;
	public int value;

	public Score(String n, int s) {
		name = n;
		value = s;
	}

	public String toString() {
		return name + " " + value;
	}

	@Override
	public int compareTo(Score s) {
		if (value != s.value) {
			return value - s.value;
		} else {
			return name.compareTo(s.name);
		}
	}
}
