import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;

public class ScoreKeeper extends Component {
	private static final long serialVersionUID = 7918034653103981691L;
	public int score, foodWidth = 15, foodHeight = 15, x, y;
	private final int FOODVALUE = 15;
	private final Color foodColor = Color.YELLOW;
	private GameScreen myGameScreen;
	private Snake mySnake;

	public ScoreKeeper(Snake sn, GameScreen gs) {
		mySnake = sn;
		myGameScreen = gs;
		moveFood();
	}

	public void eatFood() {
		score += FOODVALUE;
		mySnake.newSegToAdd = true;
		moveFood();
	}

	private void moveFood() {
		do {
			x = 15 * (int) (myGameScreen.ROWS * Math.random());
			y = 15 * (int) ((myGameScreen.COLUMNS - 2) * Math.random());
		} while (isFoodOnSnake());
		repaint();
	}

	private boolean isFoodOnSnake() {
		Rectangle foodArea = new Rectangle(x, y, foodWidth, foodHeight);
		for (Snake sn : mySnake.getSegs()) {
			if (sn.getArea().intersects(foodArea))
				return true;
		}
		return false;
	}

	public void paint(Graphics g) {
		drawFood(g);
		drawScore(g);
	}

	private void drawFood(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillOval(x, y, foodWidth, foodHeight);
		g.setColor(foodColor);
		g.fillOval(x + 1, y + 1, foodWidth - 2, foodHeight - 2);
	}

	private void drawScore(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawString("Score: " + score, 2, 12);
		g.drawString("Score: " + score, 3, 12);
	}

	public void reset() {
		score = 0;
		moveFood();
	}

	public int getScore() {
		return score;
	}

}
