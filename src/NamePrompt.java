import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JInternalFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class NamePrompt extends JInternalFrame implements ActionListener {
	private static final long serialVersionUID = -2710147995742812609L;
	private JTextField textField;
	private JTextArea textArea;
	private String name = null;

	public NamePrompt(String str) {
		super(str);
		setResizable(false);
		getContentPane().setPreferredSize(new Dimension(200, 70));
		setClosable(false);
		// Set up text area informing of high score
		textArea = new JTextArea();
		textArea.setText("Congratulations!\n" + "You got a High Score!\n"
				+ "Please enter your name below:\n");
		textArea.setEditable(false);
		// Set up text field in which to type name
		textField = new JTextField();
		textField.addActionListener(this);
		// Add text field and area to internal frame
		add(textField, BorderLayout.SOUTH);
		add(textArea);
		validate();
		pack();
		setVisible(true);
	}

	public String promptForName() {
		while (name == null) {
			textField.requestFocusInWindow();
		}
		dispose();
		return name;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (!textField.getText().replaceAll(" ", "").equals("")) {
			name = textField.getText().replaceAll(" ", "");
		}
	}
}
