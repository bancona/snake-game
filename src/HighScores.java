import java.awt.FlowLayout;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class HighScores {
	private static final String fileName = "highscores.txt";

	public static ArrayList<Score> readScores() {
		File scoreFile = new File(fileName);
		ArrayList<Score> scoresInFile = new ArrayList<Score>();
		Scanner sc = null;
		try {
			sc = new Scanner(scoreFile);
			while (sc.hasNext()) {
				String scoreLine = sc.nextLine();
				String[] scoreInfo = scoreLine.split(" ");
				scoresInFile.add(new Score(scoreInfo[0], Integer
						.parseInt(scoreInfo[1])));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} finally {
			sc.close();
		}
		return scoresInFile;
	}

	public static void writeScores(ArrayList<Score> scores) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(new File(fileName)));
			for (Score s : scores) {
				bw.write(s.toString());
				bw.newLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				bw.flush();
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public static ArrayList<Score> sortScores(ArrayList<Score> scores) {
		for (int i = 0; i < scores.size(); i++) {
			int highest = i;
			int k;
			for (k = i + 1; k < scores.size(); k++) {
				if (scores.get(highest).compareTo(scores.get(k)) < 0) {
					highest = k;
				}
			}
			scores.add(i, scores.remove(highest));
		}
		return scores;
	}

	public static void checkScore(int sc, JFrame jf) {
		if (!isHighScore(sc)) {
			return;
		}
		String name = requestName(jf);
		addScore(new Score(name, sc));
	}

	private static boolean isHighScore(int sc) {
		ArrayList<Score> scores = readScores();
		for (int i = scores.size() - 1; i >= 0; i--) {
			if (sc > scores.get(i).value) {
				return true;
			}
		}
		return false;
	}

	public static void addScore(Score s) {
		ArrayList<Score> scores = readScores();
		for (int i = 0; i < scores.size(); i++) {
			if (s.compareTo(scores.get(i)) > 0) {
				scores.add(i, s);
				scores.remove(scores.size() - 1);
				writeScores(scores);
				return;
			}
		}
	}

	public static String requestName(JFrame jf) {
		NamePrompt prompt = new NamePrompt("HighScore!");
		JPanel promptPanel = new JPanel(new FlowLayout());
		promptPanel.setOpaque(false);
		promptPanel.add(prompt);
		jf.add(promptPanel);
		String name = prompt.promptForName();
		return name;
	}

	public static void displayHighScores(JFrame jf) {
		jf.getContentPane().add(new HighScoresDisplay(jf));
		jf.validate();
	}

}
