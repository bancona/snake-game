import java.awt.Component;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class Snake extends Component implements KeyListener {
	private static final long serialVersionUID = -5190797414875801772L;

	private static final int START_X = 15, START_Y = 300,
			START_DIR = KeyEvent.VK_RIGHT;
	private GameScreen myGameScreen = null;
	private int direction = START_DIR, directionCount = 0;
	protected ArrayList<Snake> allSegs = new ArrayList<Snake>();
	protected int myPosOnSnake;
	protected Color myColor = Color.GREEN;
	protected Point prevSpot = new Point();
	protected int width = 15, height = 15, x, y;
	public ScoreKeeper myScoreKeeper;
	public boolean newSegToAdd;

	public Snake(GameScreen gs) {
		allSegs.add(this);
		myColor = Color.GREEN.darker().darker();
		myGameScreen = gs;
		myGameScreen.addKeyListener(this);
		myScoreKeeper = new ScoreKeeper(this, myGameScreen);
		myGameScreen.getContentPane().add(myScoreKeeper);
		myGameScreen.validate();
		reset();
	}

	public Snake() {
	}

	public void act() {
		directionCount = 0;
		for (Snake s : allSegs) {
			s.move(s.getNextSpot());
			if (myGameScreen.dead) {
				return;
			}
		}
		repaint();
		if (newSegToAdd) {
			newSegToAdd = false;
			addSeg();
		}
	}

	/**
	 * @return Point at top-left corner of Snake-sized area next to be occupied
	 *         by head of snake
	 */
	public Point getNextSpot() {
		int nextX = x, nextY = y;
		if (direction == KeyEvent.VK_UP) {
			nextY -= 15;
		} else if (direction == KeyEvent.VK_DOWN) {
			nextY += 15;
		} else if (direction == KeyEvent.VK_LEFT) {
			nextX -= 15;
		} else {// right
			nextX += 15;
		}
		return new Point(nextX, nextY);
	}

	/**
	 * @return whether area is contained in gameScreen and whether it is already
	 *         occupied with another segment
	 */
	public boolean checkCollision(Point pt) {
		Rectangle nextArea = new Rectangle(pt, new Dimension(width, height));
		if (!myGameScreen.getGameBounds().contains(nextArea)) {
			return true;
		}
		for (Snake s : allSegs) {
			if (pt.equals(new Point(s.x, s.y))) {
				return true;
			}
		}
		if (pt.equals(new Point(myScoreKeeper.x, myScoreKeeper.y))) {
			myScoreKeeper.eatFood();
		}
		return false;
	}

	/**
	 * @param p
	 *            Point at top-left corner of Snake-sized area next to be
	 *            occupied by snake segment
	 * @return false when snake cannot move to specified point, else true
	 */
	public void move(Point p) {
		if (checkCollision(p)) {
			myGameScreen.dead = true;
			return;
		}
		prevSpot.x = x;
		prevSpot.y = y;
		x = p.x;
		y = p.y;
	}

	@Override
	public void paint(Graphics g) {
		try {
			synchronized (allSegs) {
				for (Snake s : allSegs) {
					s.drawSeg(g);
				}
			}
		} catch (Exception e) {
		}
	}

	public void drawSeg(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillOval(x, y, width, height);
		g.setColor(myColor);
		g.fillOval(x + 1, y + 1, width - 2, height - 2);
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		int key = arg0.getKeyCode();
		if (directionCount++ == 0
				&& key != direction
				&& ((key == KeyEvent.VK_UP && direction != KeyEvent.VK_DOWN)
						|| (key == KeyEvent.VK_DOWN && direction != KeyEvent.VK_UP)
						|| (key == KeyEvent.VK_LEFT && direction != KeyEvent.VK_RIGHT) || (key == KeyEvent.VK_RIGHT && direction != KeyEvent.VK_LEFT))) {
			direction = key;
		}
	}

	public void reset() {
		synchronized (allSegs) {
			Snake head = allSegs.get(0);
			allSegs.clear();
			allSegs.add(head);
		}
		x = START_X;
		y = START_Y;
		direction = START_DIR;
		myScoreKeeper.reset();
	}

	public void addSeg() {
		allSegs.add(new SnakeSeg(this, myColor.brighter().brighter()));
	}

	/**
	 * 
	 * @return ArrayList of snake's segments
	 */
	public ArrayList<Snake> getSegs() {
		return allSegs;
	}

	/**
	 * 
	 * @return area in which snake resides
	 */
	public Rectangle getArea() {
		return new Rectangle(x, y, width, height);
	}

	/**
	 * 
	 * @return top-left corner of area in which snake resides
	 */
	public Point getSpot() {
		return new Point(x, y);
	}

	/**
	 * 
	 * @return snake segment in front of current segment, or null if head
	 */
	public Snake getNextSeg() {
		if (myPosOnSnake == 0) {
			return null;
		} else {
			return allSegs.get(myPosOnSnake - 1);
		}
	}

	/**
	 * 
	 * @return number of segments in snake
	 */
	public int getNumSegs() {
		return allSegs.size();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}
}
