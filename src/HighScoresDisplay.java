import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class HighScoresDisplay extends JPanel {

	private static final long serialVersionUID = -700061988932272992L;
	public JFrame myFrame;

	public HighScoresDisplay(JFrame jf) {
		myFrame = jf;
		setLayout(new FlowLayout());
		setOpaque(false);
		add(new DisplayWindow(this));
	}

	private class DisplayWindow extends JInternalFrame {
		private static final long serialVersionUID = -1797582284452953203L;
		private JTextArea textArea;
		private HighScoresDisplay jp;

		public DisplayWindow(HighScoresDisplay jp) {
			this.jp = jp;
			setClosable(true);
			setTitle("HIGHSCORES");
			setResizable(false);
			getContentPane().setPreferredSize(new Dimension(200, 100));
			setLocation(300, 100);
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			getContentPane().setBackground(Color.WHITE);
			textArea = new JTextArea(5, 20);
			String text = "";
			int i = 1;
			for (Score s : HighScores.readScores()) {
				text += i++ + ": " + s.toString() + "\n";
			}
			textArea.setText(text);
			textArea.setEditable(false);
			textArea.setVisible(true);
			add(textArea);
			validate();
			pack();
			setVisible(true);
		}

		@Override
		public void dispose() {
			setVisible(false);
			jp.setVisible(false);
			super.dispose();
			// jp.myFrame.remove(jp);
		}

	}
}
