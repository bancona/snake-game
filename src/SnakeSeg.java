import java.awt.Color;
import java.awt.Point;

public class SnakeSeg extends Snake {
	private static final long serialVersionUID = 5182931140939901744L;

	public SnakeSeg(Snake head, Color c) {
		allSegs = head.getSegs();
		myColor = c;
		myPosOnSnake = getNumSegs();
		Point myLoc = getNextSeg().prevSpot;
		x = myLoc.x;
		y = myLoc.y;
	}

	public boolean checkCollision(Point pt) {
		return pt.equals(getNextSeg().getSpot());
	}

	public Point getNextSpot() {
		return getNextSeg().prevSpot;
	}
}
