# Snake Game

#### By Alberto (Bertie) Ancona
#### Developed in fall 2012

## Overview
Snake is one of a series of arcade games I built for fun during my junior year of high school. It is also one of the first games I ever made, and it definitely shows. Looking back, if I could redo the game I would definitely improve its look and feel with cleaner, prettier graphics. I would also improve the highscores feature such that the game would work without the need for a pre-written [highscores.txt](./highscores.txt) (see Setup Instructions).

## Setup Instructions
### With Runnable JAR
I have included a runnable jar file to expedite setup. In order to run the game, you must also include the [highscores.txt](./highscores.txt) file, also provided, in the same directory.
### From Source
If you prefer, you can compile the Java files yourself by running the command `javac GameScreen.java` and then run the program by running the command `java GameScreen` from the [src](./src) directory. In this case, you must keep the [highscores.txt](./src/highscores.txt) file in the [src](./src) directory.